require_relative 'dictionary'

class WordChains
  def initialize
    @dicitionary = Dictionary.new
  end
  
  def run(start_word,finish_word)
    @dicitionary.select_same_length(start_word)
    @dicitionary.find_path(start_word,finish_word)
    @dicitionary.show_path
  end
end

if __FILE__ == $PROGRAM_NAME
  puts "Run 'Word Chains'?   (y/n)"
  input = String(gets.chomp.downcase)
  if input == "y"
    puts "Please type 'start' and 'finish' words like so: cat,dog"
    input = (String(gets.chomp.downcase)).split(',')
    start_word = input[0]
    finish_word = input[1]
    
    word_chains = WordChains.new
    word_chains.run(start_word,finish_word)
  end
end 