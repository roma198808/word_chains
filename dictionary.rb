require_relative 'node'
require 'set'

class Dictionary
  def initialize
    @dict        = load_dictionary
    @start_node  = nil
    @finish_node = nil
    @start_time  = nil
    @visited     = []
  end
  
  # modifies @dict to the words of the same length
  def select_same_length(word)
    puts "-> Deleting longer and shorter words from the dictionary..."
    d = @dict.select {|d_word| d_word.length == word.length }
    @dict = Set.new(d)
  end
  
  # returns node with finish word or nil
  def find_path(current_word,finish_word)
    puts "-> Finding paths..."
    @start_time = Time.now
    @start_node = Node.new(current_word)
    @visited   << current_word
    queue       = [@start_node]
    
    until queue.empty?
      current_node = queue.shift
      if current_node.value == finish_word
        @finish_node = current_node
        return current_node
      end
      
      children = similar_words(current_node,finish_word)
      children.each do |child_node|
        child_node.value == finish_word ? queue.unshift(child_node) : queue << child_node
      end
    end
    nil
  end
  
  # returns an arrayed path
  def show_path
    if @start_node == nil || @finish_node == nil
      puts "-> Result: Start or Finish nodes haven't been found"
    else
      path = [@finish_node]
      until path.first.parent.nil?
        path.unshift(path.first.parent)
      end
      puts
      puts "-> Result: #{path.map(&:value).join("  =>  ")}"
      puts "-> Time: #{(Time.now - @start_time).round(2)} seconds"
      puts
    end
  end
  
  
  
  private
  
  # loads local dictionary.txt file
  def load_dictionary
    puts "-> Loading dictionary..."
    f = File.open("dictionary.txt").map(&:strip)
    f = Set.new(f)
  end
  
  # returns children nodes or empty array
  def similar_words(node,finish_word)
    @dict.each do |el|
      if (el.split('') - node.value.split('')).count == 1
        unless @visited.include?(el)
          @visited << el
          child = Node.new(el)
          node.add_child(child)
          break if el == finish_word
        end
      end
    end
    
    node.children
  end
end 
